---
layout: handbook-page-toc
title: "Security Awards Leaderboard"
---

### On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

This page is [auto-generated and updated every Mondays](../security-awards-program.html#process).

# Leaderboard FY23

## Yearly

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@kassio](https://gitlab.com/kassio) | 1 | 600 |
| [@brodock](https://gitlab.com/brodock) | 2 | 600 |
| [@leipert](https://gitlab.com/leipert) | 3 | 500 |
| [@.luke](https://gitlab.com/.luke) | 4 | 500 |
| [@Andysoiron](https://gitlab.com/Andysoiron) | 5 | 500 |
| [@garyh](https://gitlab.com/garyh) | 6 | 440 |
| [@georgekoltsov](https://gitlab.com/georgekoltsov) | 7 | 400 |
| [@m_frankiewicz](https://gitlab.com/m_frankiewicz) | 8 | 200 |
| [@alexkalderimis](https://gitlab.com/alexkalderimis) | 9 | 120 |
| [@toupeira](https://gitlab.com/toupeira) | 10 | 120 |
| [@10io](https://gitlab.com/10io) | 11 | 100 |
| [@jerasmus](https://gitlab.com/jerasmus) | 12 | 80 |
| [@dmakovey](https://gitlab.com/dmakovey) | 13 | 80 |
| [@mwoolf](https://gitlab.com/mwoolf) | 14 | 60 |
| [@dskim_gitlab](https://gitlab.com/dskim_gitlab) | 15 | 60 |
| [@proglottis](https://gitlab.com/proglottis) | 16 | 60 |
| [@minac](https://gitlab.com/minac) | 17 | 60 |
| [@egrieff](https://gitlab.com/egrieff) | 18 | 60 |
| [@hortiz5](https://gitlab.com/hortiz5) | 19 | 60 |
| [@pshutsin](https://gitlab.com/pshutsin) | 20 | 40 |
| [@ghickey](https://gitlab.com/ghickey) | 21 | 40 |
| [@bdenkovych](https://gitlab.com/bdenkovych) | 22 | 40 |
| [@ohoral](https://gitlab.com/ohoral) | 23 | 30 |
| [@subashis](https://gitlab.com/subashis) | 24 | 30 |
| [@ebaque](https://gitlab.com/ebaque) | 25 | 30 |
| [@jprovaznik](https://gitlab.com/jprovaznik) | 26 | 20 |
| [@ntepluhina](https://gitlab.com/ntepluhina) | 27 | 20 |
| [@mikolaj_wawrzyniak](https://gitlab.com/mikolaj_wawrzyniak) | 28 | 20 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@katiemacoy](https://gitlab.com/katiemacoy) | 1 | 50 |
| [@rspeicher](https://gitlab.com/rspeicher) | 2 | 30 |

### Non-Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@vburton](https://gitlab.com/vburton) | 1 | 30 |

### Community

Category is empty

## FY23-Q1

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@kassio](https://gitlab.com/kassio) | 1 | 600 |
| [@brodock](https://gitlab.com/brodock) | 2 | 600 |
| [@leipert](https://gitlab.com/leipert) | 3 | 500 |
| [@.luke](https://gitlab.com/.luke) | 4 | 500 |
| [@Andysoiron](https://gitlab.com/Andysoiron) | 5 | 500 |
| [@garyh](https://gitlab.com/garyh) | 6 | 440 |
| [@georgekoltsov](https://gitlab.com/georgekoltsov) | 7 | 400 |
| [@m_frankiewicz](https://gitlab.com/m_frankiewicz) | 8 | 200 |
| [@alexkalderimis](https://gitlab.com/alexkalderimis) | 9 | 120 |
| [@toupeira](https://gitlab.com/toupeira) | 10 | 120 |
| [@10io](https://gitlab.com/10io) | 11 | 100 |
| [@jerasmus](https://gitlab.com/jerasmus) | 12 | 80 |
| [@dmakovey](https://gitlab.com/dmakovey) | 13 | 80 |
| [@mwoolf](https://gitlab.com/mwoolf) | 14 | 60 |
| [@dskim_gitlab](https://gitlab.com/dskim_gitlab) | 15 | 60 |
| [@proglottis](https://gitlab.com/proglottis) | 16 | 60 |
| [@minac](https://gitlab.com/minac) | 17 | 60 |
| [@egrieff](https://gitlab.com/egrieff) | 18 | 60 |
| [@hortiz5](https://gitlab.com/hortiz5) | 19 | 60 |
| [@pshutsin](https://gitlab.com/pshutsin) | 20 | 40 |
| [@ghickey](https://gitlab.com/ghickey) | 21 | 40 |
| [@bdenkovych](https://gitlab.com/bdenkovych) | 22 | 40 |
| [@ohoral](https://gitlab.com/ohoral) | 23 | 30 |
| [@subashis](https://gitlab.com/subashis) | 24 | 30 |
| [@ebaque](https://gitlab.com/ebaque) | 25 | 30 |
| [@jprovaznik](https://gitlab.com/jprovaznik) | 26 | 20 |
| [@ntepluhina](https://gitlab.com/ntepluhina) | 27 | 20 |
| [@mikolaj_wawrzyniak](https://gitlab.com/mikolaj_wawrzyniak) | 28 | 20 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@katiemacoy](https://gitlab.com/katiemacoy) | 1 | 50 |
| [@rspeicher](https://gitlab.com/rspeicher) | 2 | 30 |

### Non-Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@vburton](https://gitlab.com/vburton) | 1 | 30 |

### Community

Category is empty


